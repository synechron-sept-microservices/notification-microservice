package com.synechron.notificationsservice;

import com.synechron.notificationsservice.model.Loan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LoanNotificationService {

    @StreamListener(Sink.INPUT)
    public void sendNotification(Loan loan){
        log.info("Sending an email for loan processing ");
        log.info(loan.toString());
    }
}