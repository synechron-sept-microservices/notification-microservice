package com.synechron.notificationsservice.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class Notification {

    private String id;

    private String emailAddress;

    private String message;

}