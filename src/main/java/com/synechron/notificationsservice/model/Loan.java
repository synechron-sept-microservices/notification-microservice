package com.synechron.notificationsservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Loan {
    private String loanId;

    private double loanAmount;

    private double balanceAmount;

    private int balanceTenure;

    private int totalTenure;

    private String status;


}