package com.synechron.notificationsservice.controller;

import com.synechron.notificationsservice.model.Notification;
import org.springframework.web.bind.annotation.*;
import sun.misc.UUDecoder;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/api/notifications/")
public class NotificationController {

    @PostMapping
    public Notification publish(@RequestBody Notification notification){
        notification.setId(UUID.randomUUID().toString());
        return notification;
    }

    @GetMapping
    public Set<Notification> fetch(){
        return new HashSet<>(
                Arrays.asList(
                        Notification.builder().id(UUID.randomUUID().toString()).emailAddress("ram@gmail.com").message("test").build(),
                        Notification.builder().id(UUID.randomUUID().toString()).emailAddress("vinayak@gmail.com").message("one more").build()
                ));

    }
}